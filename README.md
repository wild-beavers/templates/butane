# Butane template for UNDEFINED

> Briefly describes what this project is about

## Variables to replace

Before using the ignition configuration, one must replace these values in the configuration:

> All variables needed by this butane code
> e.g.
>
> `VPN_SSM_PARAMETER_PRIVATE_KEY_NAME`: the SSM Parameter name that will contain the machine(s) private key.
> `VPN_SSM_PARAMETER_PUBLIC_KEY_NAME`: the SSM Parameter name that will contain the machine(s) public key.
> `VPN_PORT`: the UDP port for the VPN network interface.
